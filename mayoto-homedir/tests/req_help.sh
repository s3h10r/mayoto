#!/bin/bash

if [[ -z "$TEST_MAILTO" ]]
then
  echo "please set TEST_MAILTO to mayoto's e-mail first"
  echo "eg., export TEST_MAILTO='mayoto@example.com'"
  exit 1
fi

LOGDIR="$(echo ~/log)"
MBOX="$(echo ~/Mail/plain)"

echo "request available tasks by sending mail to $TEST_MAILTO"
cat /dev/null | mail -s "help" $TEST_MAILTO

RC=$?
echo "RC: $?"

read -p "press any key to launch logview"

multitail -i $LOGDIR/procmail.log -i $LOGDIR/task_help.log $LOGDIR/aclctrl.log
mutt -f $MBOX
