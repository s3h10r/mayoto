#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
helper funcs
"""
from z_execCmd import execCmd

def grab_doc(fn=None, bin_path="./"):
    """
    grab __doc__ out of python-file
    """
    is_py = fn.split('.')[-1] == 'py'
    if not is_py:
        return ''

    rc,out = execCmd('pydoc %s/%s' % (bin_path,fn))

    if rc != 0:
        raise Exception, "failed to grab docstring"

    return ''.join(out)


def gen_uid():
    """
    TODO generate an unique task-id
    """
    return None

if __name__ == '__main__':
    print "TODO"

