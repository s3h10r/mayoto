#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
list available tasks

[X] ACL support

usage:

    mail-subject format: "help [arg1] [arg2]"
"""

import os,sys

import email
from email.utils import parseaddr

import logging
LOGFILE = "/home/mayoto/log/task_help.log"
logging.basicConfig(
    filename=LOGFILE,
    level=logging.DEBUG,
#    level=logging.INFO,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("task_help")

from z_acl import is_known_user, is_allowed_user, acl_violation_mail, avail_tasks
from z_send_mail import send_mail

CHECK_ACL=True

__version__ = '0.0.1'
__license__ = "GPL"

if __name__ == '__main__':

    myname = os.path.basename(sys.argv[0])

    logger.debug('got triggered...')

    msgtxt = sys.stdin.read() # read piped in data
    msg = email.message_from_string(msgtxt)

    you = msg['From']
    you = parseaddr(you)[-1]
    you_firstname = you.split('@')[0].split('.')[0]
    me = msg['To']
    me = parseaddr(me)[-1] 
    subj = msg['Subject']
    msg_id = msg['Message-ID']

    print >> sys.stderr, "%s : this should go to procmail.log" % myname

    logger.info("received msg_id %s from: %s to: %s" % (msg_id,you,me)) 
    logger.info("subject: %s" % (subj)) 
    print >> sys.stderr, "received msg_id %s from: %s to: %s" % (msg_id,you,me)
    logger.info("subject: %s" % (subj)) 

    if CHECK_ACL:
        if not is_allowed_user(you,myname):
            logger.warning("task %s denied for user %s" % (myname,you))
            if not is_known_user(you): # fail silently
                logger.critical("request for '%s' from unknown user: %s" % (myname,you))
                sys.exit(0)
            else:
                # post access deny
                acl_violation_mail(me,you,myname,subj)
                sys.exit(0)

    body_txt = "hi %s!\n" % you_firstname
    body_txt += "avail_tasks: \n%s" % avail_tasks(you)


    send_mail(me, [you], "Re: %s" % subj, body_txt, [], server='localhost')        

    logger.debug('going back to sleep now.') 
