#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
process mails received (via procmail)
send auto-reply to sender

also a kind of simple "template" for building other action_scripts.
"""

"""
let procmail log some basic infos by printing things to stderr
--------------------------------------------------------------

   print >> sys.stderr, "%s : this should go to procmail.log" % os.path.basename(sys.argv[0])

use logger for detailed logging
-------------------------------

    logger.info("bla")
"""


import os,sys

import email
from email.utils import parseaddr
from email.mime.text import MIMEText

import smtplib # sending mail

import logging
LOGFILE = "/home/mayoto/log/task_selftest.log"
logging.basicConfig(
    filename=LOGFILE,
    level=logging.DEBUG,
#    level=logging.INFO,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("action_selftest")

__version__ = '0.0.1'
__license__ = "GPL"

if __name__ == '__main__':

    myname = os.path.basename(sys.argv[0])

    logger.debug('got triggered...')

    msgtxt = sys.stdin.read() # read piped in data
    msg = email.message_from_string(msgtxt)

    you = msg['From']
    # http://stackoverflow.com/questions/550009/parsing-from-addresses-from-email-text
    you = parseaddr(you)[-1]
    you_firstname = you.split('@')[0].split('.')[0]

    me = msg['To']
    me = parseaddr(me)[-1] 

    subj = msg['Subject']
    msg_id = msg['Message-ID']

    print >> sys.stderr, "%s : this should go to procmail.log" % myname

    logger.info("received msg_id %s from: %s to: %s" % (msg_id,you,me)) 
    logger.info("subject: %s" % (subj)) 

    print >> sys.stderr, "received msg_id %s from: %s to: %s" % (msg_id,you,me)
    logger.info("subject: %s" % (subj)) 

    logger.debug('msg.keys(): %s' % (msg.keys())) 

    msg_repl = MIMEText("hi %s! it works. feel free to contact me. :)" % you_firstname)
    msg_repl['Subject'] = 'Re: %s' % subj
    msg_repl['From'] = me
    msg_repl['To'] = you

    s = smtplib.SMTP('localhost')
    s.sendmail(me, you, msg_repl.as_string())
    s.quit()

    logger.debug('going back to sleep now.') 
