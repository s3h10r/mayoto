#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
helper-funcs for executing external commands via shell

usage:

    <me> test
"""

import logging
LOGFILE = "/tmp/bla.log"
logging.basicConfig(
#    filename=LOGFILE,
    level=logging.DEBUG,
#    level=logging.INFO,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("z_execCmd")

__author__  = 'sven.hessenmueller@gmail.com'
__version__ = '0.0.2'
__license__ = "GPL"


def execCmd(sCmd,env={}):
    """
    executes a given Command

    returns
        rc, l_output_console
    """
    import subprocess as sub

    p = sub.Popen([sCmd], shell=True, stdout=sub.PIPE, stderr=sub.STDOUT, env=env)

    #output = p.stdout.read()
    output = p.stdout.readlines()
    p.wait()
    rc = p.returncode

    if rc != 0:
        logger.critical("command failed with rc %i: %s" % (rc,sCmd))
        logger.critical("got %s" % (output))

    return rc, output


def execCmds(lCmd,env={},stop_on_error=True):
    """
    executes list of commands 

    returns
        rc, l_returncodes, l_output_console
    """
    rc, l_rc, l_output = None,[],[]
   
    for cmd in lCmd:
        rc, output = execCmd(cmd,env)
        l_rc.append(rc)
        l_output.append(output)

        if rc != 0:
            logger.critical("command failed with rc %i: %s" % (rc,cmd))
            logger.critical("got %s" % (output))
            if stop_on_error:
                break
            else:
                continue

    return rc, l_rc, l_output


def _selftest():
    """
    TODOP3: assert...
    """

    import tempfile
    fn_nothere = tempfile.mktemp()
    lCmd = ['ls -l', 'du -h', 'cat %s' % fn_nothere, 'true', 'false', 'df -h' ]

    print "="*68
    print "test: stop_on_error=False"
    rc, lrcs, lout = execCmds(lCmd,{},stop_on_error=False)
    for i,cmd in enumerate(lCmd):
        print "-"*68 
        print "cmd '%s' rc: %i" % (lCmd[i],lrcs[i])
        print "%s" % ''.join(lout[i])

    print "="*68
    print "test: stop_on_error=True"
    rc, lrcs, lout = execCmds(lCmd,{},stop_on_error=True)
    for i,cmd in enumerate(lCmd):
        print "-"*68
        try:
            print "cmd '%s' rc: %i" % (lCmd[i],lrcs[i])
            print "%s" % ''.join(lout[i])
        except IndexError:
            print "cmd '%s' rc: not in list" % (lCmd[i])

if __name__ == '__main__':
    import os,sys

    if len(sys.argv) > 1:
        _selftest()
    else:
        print os.path.basename(sys.argv[0]), __version__
        print __doc__

