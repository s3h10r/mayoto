#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
helper-funcs for sending mail

source:
http://stackoverflow.com/questions/3362600/how-to-send-email-attachments-with-python
"""

__version__ = "0.0.1"

def send_mail(send_from, send_to, subject, text, files=[], server="localhost"):
    """
    send mail with attachments
    source: http://stackoverflow.com/questions/3362600/how-to-send-email-attachments-with-python
    """
    import smtplib, os

    from email.MIMEMultipart import MIMEMultipart
    from email.MIMEBase import MIMEBase
    from email.MIMEText import MIMEText
    from email.Utils import COMMASPACE, formatdate
    from email import Encoders



    assert type(send_to)==list
    assert type(files)==list

    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach( MIMEText(text) )

    for f in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(f,"rb").read() )
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(f))
        msg.attach(part)

    smtp = smtplib.SMTP(server)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()


if __name__ == '__main__':
    import os,sys

    if len(sys.argv) > 1:
        _selftest()
    else:
        print os.path.basename(sys.argv[0]), __version__
        print __doc__

