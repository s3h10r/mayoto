#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
option to wrap tasks with ACLs

tasks which do not implement ACL-checks inline could be put
under ACL-control

usage:

    ./aclctrl.py <taskname>

example (procmail):

    # task grab_audio from youtube-vid
    :0 c:yt2mp3.lock
    * ^Subject:.*grab_audio*
    | $ACLCTRL $ACT_YT2MP3
"""

import os,sys
import email
from email.utils import parseaddr

import logging
LOGFILE = "/home/mayoto/log/aclctrl.log"
logging.basicConfig(
    filename=LOGFILE,
    level=logging.DEBUG,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("aclctrl")

#CHECK_ACL=False # default, good for testing/debugging tasks
CHECK_ACL=True

from z_acl import is_known_user, is_allowed_user, acl_violation_mail, avail_tasks
from z_send_mail import send_mail

__version__ = '0.0.1'
__license__ = "GPL"

def run_task(taskbin="./bin/task_selftest.py",msg="blabla"):
    import subprocess

    try:
        tproc = subprocess.Popen([taskbin], stdin=subprocess.PIPE, stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    except Exception, err:
        logger.critical("run_task %s : %s" % (taskbin,err))
        return 254

    tproc.stdin.write(msg)
    tproc.stdin.close()
    rc = tproc.wait()
    out = tproc.stdout.read()
    out_err = tproc.stderr.read()
    tproc.stdout.close()
    tproc.stderr.close()

    if rc != 0:
        logger.critical("run_task %s returned %i" % (taskbin,rc))
        logger.critical("stderr: %s" % out_err)
    return rc


if __name__ == '__main__':
    logger.debug('got triggered...')
    myname = os.path.basename(sys.argv[0])
    if not len(sys.argv) > 1:
        print myname,__version__
        print __doc__
        sys.exit(0)

    req_taskbin = sys.argv[1] # full path
    req_task = os.path.basename(req_taskbin)

    msgtxt = sys.stdin.read() # read piped in data

    if not CHECK_ACL: # just cut-through

        # good for testing if the tasks work but nothing for production-servers
        logger.warning("---!!! THIS IS VERY DANGEROUS!!! --- CHECK_ACL is set to False!")

        rc = run_task(req_taskbin,msgtxt) 
        sys.exit(rc)

    msg = email.message_from_string(msgtxt)
    you = msg['From']
    you = parseaddr(you)[-1]
    you_firstname = you.split('@')[0].split('.')[0]
    me = msg['To']
    me = parseaddr(me)[-1] 
    subj = msg['Subject']
    msg_id = msg['Message-ID']

    if not is_allowed_user(you,req_task):
        logger.warning("task %s denied for user %s" % (req_task,you))
        if not is_known_user(you): # fail silently
            logger.critical("request for '%s' from unknown user: %s" % (req_task,you))
            sys.exit(0)
        else:
            # post access deny
            acl_violation_mail(me,you,os.basename(req_task),subj)
            sys.exit(0)

    # spawn task and feed with mail
    run_task(req_taskbin,msgtxt) 


    logger.debug('going back to sleep now.') 
