#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
helper funcs for access-control
"""

import logging
LOGFILE = "/home/mayoto/log/z_acl.log"
logging.basicConfig(
    filename=LOGFILE,
    level=logging.INFO,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("z_acl")


from z_send_mail import send_mail
from z_taskhlp import grab_doc

# --- read config
import os,sys
fn_conf = "/etc/mayoto/z_acl.conf"
if not os.path.isfile(fn_conf):
    fn_conf = "%s/z_acl.conf" % (os.path.dirname(sys.argv[0])) #dirty, could be sys.argv of the "importer", e.g. task_help.py
config = {}
execfile(fn_conf, config)
# --- 


def is_known_user(user="foo.bar@example.com"):
    
    return config["ACL"].has_key(user)

def is_allowed_user(user="foo@example.com", task="selftest"):
    """
    check if user is allowed for given task
    """
    ALLOW=False

    if config['ACL'].has_key(user):
        if config['ACL'][user] == [] or task in config['ACL'][user]:
            ALLOW=True

    logger.info("is_allowed_user(%s,%s) : %s" % (user,task,ALLOW))

    return ALLOW


def acl_violation_mail(me='mayoto.example.com', user='foo.bar@example.com', task=None, subj=None):
    """
    send access deny stuff
    """

    body_txt="nope. sorry, you're not allowed to do that. task: %s #ACL-conf\n" % task
    body_txt += "avail_tasks for you are: %s" % avail_tasks(user)

    send_mail(me, [user], "Re: %s" % subj, body_txt, [], server='localhost')


def avail_tasks(user=None):
    """
    list available tasks

    if no user is given, show all tasks available otherwise only those
    which user has access to. (ACL)
    """
    import glob

    mydir = os.path.dirname(sys.argv[0]) #dirty, could be sys.argv of the "importer", e.g. task_help.py
    all_tasks = []
    for taskbin in glob.iglob("%s/task_*" % mydir):
        if taskbin.split('.')[-1] != 'pyc':
            all_tasks.append(os.path.basename(taskbin))

    if user:
        if config['ACL'].has_key(user):
            if config['ACL'][user] == []: # all access
                return all_tasks
            else:    
                return config['ACL'][user]
        else:
            return None

    return all_tasks


if __name__ == '__main__':
    print "--- active ACLs (%s):" % fn_conf
    for k,v in config['ACL'].items():
        print k
        print " "*4, v

    print "--- avail_tasks :"
    print "--- %s" % (os.path.dirname(os.path.abspath(sys.argv[0])))
    for fn in avail_tasks():
        print fn
        #print grab_doc(fn)
        


