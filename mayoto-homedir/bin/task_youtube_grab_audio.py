#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
extract audio from youtube-video and mail it back (or a link to it)

wraps youtube-dl, ffmpeg, lame

mail-subject format:

    "grab_audio <valid_youtube_url> [as_link] [{mp3|...}]

mail-subject example:

    "grab_audio http://www.youtube.com/watch?v=thisshouldreallyproduceanerrormsg as_link mp3"

    # Neuro - Die Ewigkeit schmerzt, Evoke 2006 1st place
    "grab_audio http://www.youtube.com/watch?v=x06nS3rtxuI"
"""

"""
!!! NO ACL_inline support, please use aclctrl-wrapper

TODO:
[X] get_title of video 
[ ] option to get_title via mail-body, e.g. in format 'artist:title'
[X] title of video as filename
[ ] put meta-infos into audio-file
"""

import os,sys,tempfile,time
import email

import logging
LOGFILE = "/home/mayoto/log/task_yt2audio.log"
logging.basicConfig(
    filename=LOGFILE,
    level=logging.INFO,
    format= "%(asctime)s [%(levelname)-8s] %(message)s" )

logger = logging.getLogger("task_yt2audio")

from z_send_mail import send_mail
from z_execCmd import execCmd, execCmds
from z_taskhlp import gen_uid #TODO

DEBUG=True 
TO_AUDIO_FORMAT = 'mp3' # target audio format
RESULT_AS_MAIL = True 
DIR_OUT='/home/mayoto/out/' # place to put result in if RESULT_AS_MAIL=False
DIR_TMP='/home/mayoto/tmp/'

DEBUG=True 

BIN_YTDL='/usr/local/bin/youtube-dl'
BIN_FFMPEG='/usr/bin/ffmpeg'
BIN_LAME='/usr/bin/lame'

if DEBUG:
    BIN_YTDL="%s -v" % BIN_YTDL # verbose output

__version__ = '0.0.2'
__license__ = "GPL"


def _cleanup(fns = []):
    """
    delete given filenames
    """
    for fn in fns:
        try:
            os.remove(fn)
        except:
            logger.warning('could not delete file: %s' % fn)


if __name__ == '__main__':

    logger.debug('got triggered...')

    myname = os.path.basename(sys.argv[0])

    msgtxt = sys.stdin.read() # read piped in data
    msg = email.message_from_string(msgtxt)

    you = msg['From']
    me = msg['To']
    subj = msg['Subject']
    msg_id = msg['Message-ID']

    logger.info("received msg_id %s from: %s to: %s" % (msg_id,you,myname))
    logger.debug('msg.keys(): %s' % (msg.keys()))

    print >> sys.stderr, "%s : subject: '%s'" % (myname, subj) # should go to procmail's log automatically

    task_elems = subj.split()

    if task_elems[-1] == 'as_link':
        RESULT_AS_MAIL=False

    URL=task_elems[1] # stuff to download
    URL_VALID=False
    # --- check if we got a valid url (and not a shell-injection or alike...)
    import urllib
    try:
        urllib.urlopen(URL)
        URL_VALID=True
    except IOError:
        logger.critical('not an (existing) url: %s' % URL)
        body_txt = "sorry, the requested URL '%s' seems to be broken... :-(" % URL
        send_mail(me, [you], "Re: %s" % subj, body_txt, [], server='localhost')
        sys.exit(1)
    # ---

    TMP_DIR="/tmp/"

    fn_vid = tempfile.mktemp(dir=DIR_TMP)
    fn_audio = "%s.%s" % (fn_vid,TO_AUDIO_FORMAT) # resulting filename
    fn_audio_tmp = "%s.wav" % (fn_vid) 

    fn_report = "%s.txt" % tempfile.mktemp() 
    f_report = open(fn_report,'a') # append or create

    # --- get title (at least in report, TODO: put title into filename)   
    cmd = '%s --get-title %s'  % (BIN_YTDL,URL)

    start_time = time.time()
    rc,console = execCmd(cmd)
    duration_sec = time.time() - start_time

    
    if rc == 0: # build filename on result
        # but replace all those freakin' special characters 
        fn_audio = console[-1].strip().replace(' ','_')
        import re
        fn_audio = re.sub('[^a-zA-Z0-9_\n\.]', '', fn_audio)
        fn_audio = "%s%s.%s" % (DIR_TMP,fn_audio,TO_AUDIO_FORMAT)


    print >> f_report, 'duration: %i sec' % duration_sec
    print >> f_report, 'cmd: %s' % cmd
    print >> f_report, '\n'.join(console)
    print >> f_report, '-'*68
    # ---

    cmd1 = '%s --output "%s" %s'  % (BIN_YTDL,fn_vid, URL)
    cmd2 = '%s -i %s %s'  % (BIN_FFMPEG,fn_vid,fn_audio_tmp)
    cmd3 = '%s %s %s'  % (BIN_LAME,fn_audio_tmp, fn_audio)
    cmds = [cmd1,cmd2,cmd3]

    start_time = time.time()
    rc,l_rc,l_console = execCmds(cmds)
    duration_sec = time.time() - start_time

    print >> f_report, 'duration: %i sec' % duration_sec
    print >> f_report, 'cmds: %s' % cmds
    for console in l_console:
        print >> f_report, '\n'.join(console)        
    print >> f_report, '-'*68
    f_report.close()


    body_err = ""
    if rc == 0 and not os.path.isfile(fn_audio):
        body_err="FIXME: rc == 0 and not os.path.isfile(fn_audio)"
        rc = 2

    if rc == 0:
        if RESULT_AS_MAIL:
            body_txt = "hi, here's your audio-file. took me %i sec. \n" % duration_sec
            subj = "Re: %s" % subj
            send_mail(me, [you], subj, body_txt, [fn_audio,fn_report], server='localhost')
        else:
            body_txt = "hi, sending you an URL to the result is another TODO. %s,%s. took me %i sec. \n" % (os.path.basename(fn_audio),os.path.basename(fn_report),duration_sec)
            subj = "Re: %s" % subj
            send_mail(me, [you], subj, body_txt, [], server='localhost')
        logger.info("success. sent mail.")

    else:
        subj = "Re: %s - ERROR" % subj
        body_txt = '*Ooops*, something went wrong ant it took me %i sec. \n\n' % duration_sec
        body_txt += body_err
        body_txt += '\n'.join(console)        
        send_mail(me, [you], subj, body_txt, [fn_report], server='localhost')
        logger.info("failure. sent mail about issue.")
  
    if RESULT_AS_MAIL:
        _cleanup([fn_audio,fn_vid,fn_audio_tmp,fn_report])
    else:
        _cleanup([fn_vid,fn_audio_tmp])
        # move fn_audio, fn_report to output-dir
        os.rename(fn_audio,"%s/%s" % (DIR_OUT,os.path.basename(fn_audio)))
        os.rename(fn_report,"%s/%s" % (DIR_OUT,os.path.basename(fn_report)))


    logger.debug('going back to sleep.')

