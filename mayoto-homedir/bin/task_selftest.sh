#!/bin/bash
# triggered via ~/.procmailrc if mail to trigger-smtp@selftest.querzone.de
# arrives

RCPT="recipient@example.com"

print_date() {
   date '+%m%d%Y_%H%M%p'
}

mail_reply() {
  COOKIE="Kein Glück ist vom Glück der anderen getrennt."
  echo "$COOKIE" | mail -s "$0" $RCPT # TODOP2 reply to sender instead
}

NOW=`print_date`

# catching input (==mail)
# and doing nothing with it for now :)
INPUT=""
while read LINE
do
INPUT="${INPUT}\n${LINE}"
done


`mail_reply`

