"~/.vimrc

"--- PYTHON stuff ---
"enter spaces when tab is pressed
set expandtab
" use 4 spaces to represent a tab
set tabstop=4
set softtabstop=4
"nr of space to use for autoindent
set shiftwidth=4
set smarttab
" set foldmethod good for python
set foldmethod=indent

